<?php

namespace Melia\Uuid\Reference\Uuid\Generator;

use Melia\Uuid\Common\Uuid\Generator\UuidGenerationContext as UuidGenerationContextInterface;

/**
 * Implementation of UuidGenerationContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UuidGenerationContext implements UuidGenerationContextInterface {
    /**
     * Version
     *
     * @var integer
     */
    private $version;
    /**
     * Node
     *
     * @var string
     */
    private $node;
    /**
     * Namespace
     *
     * @var string
     */
    private $namespace;

    /**
     * Constructor
     *
     * @param integer $version            
     * @param string $node            
     * @param string $namespace            
     */
    public function __construct($version, $node = null, $namespace = null) {
        $this->setVersion($version)->setNode($node)->setNamespace($namespace);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\Generator\UuidGenerationContext::getVersion()
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * Set version
     *
     * @param integer $version            
     * @return \Melia\Uuid\Reference\Uuid\Generator\UuidGenerationContext
     */
    public function setVersion($version) {
        $this->version = $version;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\Generator\UuidGenerationContext::getNode()
     */
    public function getNode() {
        return $this->node;
    }

    /**
     * Set node
     *
     * @param string $node            
     * @return \Melia\Uuid\Reference\Uuid\Generator\UuidGenerationContext
     */
    public function setNode($node) {
        $this->node = $node;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\Generator\UuidGenerationContext::getNamespace()
     */
    public function getNamespace() {
        return $this->namespace;
    }

    /**
     * Set namespace
     *
     * @param string $namespace            
     * @return \Melia\Uuid\Reference\Uuid\Generator\UuidGenerationContext
     */
    public function setNamespace($namespace) {
        $this->namespace = $namespace;
        return $this;
    }
}