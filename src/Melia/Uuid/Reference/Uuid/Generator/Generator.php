<?php

namespace Melia\Uuid\Reference\Uuid\Generator;

use Webpatser\Uuid\Uuid;
use Melia\Uuid\Common\Uuid\Generator\UuidGenerationContext;
use Melia\Uuid\Common\Uuid\Generator\GeneratorInterface;

/**
 * Implementation of Generator
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class Generator implements GeneratorInterface {
    /**
     * Default uuid generation context
     *
     * @var UuidGenerationContext
     */
    private $defaultUuidGenerationContext;

    /**
     * Constructor
     *
     * @param UuidGenerationContext $context
     */
    public function __construct(UuidGenerationContext $context) {
        $this->setDefaultUuidGenerationContext($context);
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\Generator\GeneratorInterface::getDefaultUuidGenerationContext()
     */
    public function getDefaultUuidGenerationContext() {
        return $this->defaultUuidGenerationContext;
    }

    /**
     * Set default uuid generation context
     *
     * @param UuidGenerationContext $context
     * @return \Melia\Uuid\Reference\Factory\Factory
     */
    public function setDefaultUuidGenerationContext(UuidGenerationContext $context) {
        $this->defaultUuidGenerationContext = $context;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\GeneratorInterface::generate()
     */
    public function generate(UuidGenerationContext $context = null) {
        if(null === $context) {
            $context = $this->getDefaultUuidGenerationContext();
        }
        return (string)Uuid::generate($context->getVersion(), $context->getNode(), $context->getNamespace());
    }
}