<?php

namespace Melia\Uuid\Reference\Validator;
use Webpatser\Uuid\Uuid;

/**
 * Implementation of Validator
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *
 */
class Validator {

    /**
     * Determine wether uuid is supported
     *
     * @param string $uuid
     * @return boolean
     */
    public static function isSupportedUuid($uuid) {
        return is_string($uuid) && (36 === strlen($uuid)) && Uuid::validate($uuid);
    }
}