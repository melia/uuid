<?php

namespace Melia\Uuid\Common\Uuid;

/**
 * Implementation of SerialAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface SerialAwareInterface {

    /**
     * Retrieve serial
     *
     * @return string
     */
    public function getSerial();
}