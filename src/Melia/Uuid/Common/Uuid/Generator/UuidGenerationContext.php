<?php

namespace Melia\Uuid\Common\Uuid\Generator;

/**
 * Interface of UuidGenerationContext
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface UuidGenerationContext {

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion();

    /**
     * Get node
     *
     * @return string
     */
    public function getNode();

    /**
     * Get namespace
     *
     * @return string
     */
    public function getNamespace();
}