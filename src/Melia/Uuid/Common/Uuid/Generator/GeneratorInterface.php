<?php

namespace Melia\Uuid\Common\Uuid\Generator;

use Webpatser\Uuid\Uuid;

/**
 * Interface of GeneratorInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface GeneratorInterface {

    /**
     * Get default uuid generation context
     *
     * @return UuidGenerationContext
     */
    public function getDefaultUuidGenerationContext();

    /**
     * Generate uuid
     *
     * @param UuidGenerationContext $context            
     * @return string
     */
    public function generate(UuidGenerationContext $context = null);
}