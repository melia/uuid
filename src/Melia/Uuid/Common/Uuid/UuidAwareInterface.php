<?php

namespace Melia\Uuid\Common\Uuid;

/**
 * Implementation of UuidAwareInterface
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
interface UuidAwareInterface {

    /**
     * Retrieve uuid
     *
     * @return string
     */
    public function getUuid();
}